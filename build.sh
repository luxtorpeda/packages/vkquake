#!/bin/bash

source env.sh

set -x
set -e

log_environment
prepare_dist_dirs "$STEAM_APP_ID_LIST"

# build vkQuake
#
pushd "source/Quake"
make -j "$(nproc)" USE_CODEC_MP3=0
popd

mkdir -p "2310/dist/share/quake/id1"
mkdir -p "2310/dist/license/"
cp -v "source/Quake/vkquake"  "2310/dist/"
cp -v "vkquake.sh"            "2310/dist/"
cp -v "default.lux.cfg"       "2310/dist/share/quake/"
ln -s "../../../Id1/PAK0.PAK" "2310/dist/share/quake/id1/pak0.pak"
ln -s "../../../Id1/PAK1.PAK" "2310/dist/share/quake/id1/pak1.pak"
cp -v "source/LICENSE.txt"    "2310/dist/license/LICENSE.vkquake.txt"
